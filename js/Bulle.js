// Classe bull pour l'a imation de la page d'acceuil

class Bull {
  constructor(canvas) {
  	this.canvas = canvas;
    this.x = 0;
    this.y = 0;
    this.r = 0;
    this.g = 0;
    this.b = 0;
    this.vx = 0;
    this.vy = 0;
    this.a = 0.0;
    this.aS = Math.random()/100;
    this.xMax = this.canvas.getAttribute("width");
  	this.yMax = this.canvas.getAttribute("height");
  	this.xMax = this.xMax.substr(0, this.xMax.length - 2);
  	this.yMax = this.yMax.substr(0, this.yMax.length - 2);

  }

  afficher(context) {
  	this.a += this.aS;

    context.beginPath();

  	context.fillStyle = "rgba(" + this.r + "," + this.g + "," + this.b  + ", " + this.a + ")";

  	context.arc(this.x, this.y, 5, 0, 2*Math.PI);
  	context.fill();
  	context.closePath();

  	if(this.a >= 1.0)
  		this.aS *= -1;
  	if(this.a <= 0.0)
  		this.init();
  }

  deplace(){
  	// ajoute le vecteur de déplacement à la position
  	this.x += this.vx;
  	this.y += this.vy;

  	// test si la bulle sort du canvas on la met de l'autre côté
  	if(this.x > this.xMax)
  		this.x = 1;
  	else if(this.x < 0)
  		this.x = this.xMax - 1;

  	if(this.y > this.yMax)
  		this.y = 1;
  	else if(this.y < 0)
  		this.y = this.yMax - 1;
  }

  init(){
  	// Génère une couleur aléatoir
  	var r = Math.floor((Math.random() * 1000 )%206) + 50;
	  var g = Math.floor((Math.random() * 1000 )%206) + 50;
  	var b = Math.floor((Math.random() * 1000 )%206) + 50;
  	// Génère un vecteur de déplacement aléatoire
  	var vx = ((Math.random()*100)%2) - 1;
  	var vy = ((Math.random()*100)%2) - 1;		
  	// Génère une position aléatoire
  	var x = Math.floor((Math.random() * 10000 )%this.xMax);
  	var y = Math.floor((Math.random() * 10000 )%this.yMax);

  	// Affecte les valeur généré à l'objet
  	this.x = x;
    this.y = y;
    this.r = r;
    this.g = g;
    this.b = b;
    this.vx = vx;
    this.vy = vy;
    this.a = 0.0;
    this.aS = Math.random()/100;
  }

  majTailleMax(){
    this.xMax = this.canvas.getAttribute("width");
  	this.yMax = this.canvas.getAttribute("height");
  	this.xMax = this.xMax.substr(0, this.xMax.length - 2);
  	this.yMax = this.yMax.substr(0, this.yMax.length - 2);
  }
}
