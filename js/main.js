$(document).ready(function(){


	// Lance l'animation du fond
	animFond();

	// Mise en place de mon nom
	var monnom = $("#monnom");
	var mnWidth = Math.floor(monnom.width() + 1);
	var mnHeight = monnom.height();
	
	$("#monnom h1").hide();
	monnom.css("height", mnHeight + "px");
	
	for(var i = 0; i <= mnWidth; i++){
		// A la dernière itération on remet le text et on lance une animation dessus
		if(i == mnWidth)
			setTimeout(function(){
				$("#monnom h1").show();
				$("#monnom h1").addClass("animated").addClass("zoomInDown");
				setTimeout(function(){
					$("#monnom h1").removeClass("animated").removeClass("zoomInDown");
				}, 1000);
			}, 2 * mnWidth + 500);
			
		// Fonction qui va modifier la taille de la div
		animMonNom(i);
	}

	$("#mouton").click(function(){
		var mouton = $("#mouton");
		var audio = new Audio('./sound/mouton.wav');
		audio.play();
		if(!(mouton.hasClass("dep"))) {
			mouton.addClass("dep");

			var leftMax = 	mouton.css("left");
			leftMax = leftMax.substr(0, leftMax.length - 2);

			for(var i = leftMax; i > 10.0; i--){
				animMoutonAller(i);
			}

			for(var i = 10; i < leftMax; i++){
				animMoutonRetour(i);
			}
		}

		function animMoutonAller(left){
			setTimeout(function(){
				mouton.css("left", left);
				if(Math.floor(left%20) == 0){
					mouton.css("transform", "rotate(5deg)")
				}
				if(Math.floor(left%20) == 9){
					mouton.css("transform", "rotate(-5deg)")
				}
				if(left <= 11	)
					document.getElementById("mouton").setAttribute("src", "./img/moutonRet.png");

			}, 20 * (leftMax - left));
		}
		function animMoutonRetour(left){
			setTimeout(function(){
				mouton.css("left", left);
				if(Math.floor(left%20) == 0){
					mouton.css("transform", "rotate(5deg)")
				}
				if(Math.floor(left%20) == 9){
					mouton.css("transform", "rotate(-5deg)")
				}

				if(left >= leftMax - 1){
					mouton.removeClass("dep");
					document.getElementById("mouton").setAttribute("src", "./img/mouton.png");
				}
			}, 20 * (left - 30) + leftMax*21);
		}

	});

	// prépare les animation du survol des boutons du menu
	$("#listMenu a").css("font-size", ($(window).width() * 0.016) + "px");

	majPosAnimMenu();

	$(".unMenu").hover(function(){
		var widthMax = $(this).outerWidth();
		$(".animMenu", this).show();
		$(".animMenu", this).css("width", "0px");

		for(var width = 0; width <= widthMax; width++){
			animMenu(this, width);
		}

	
	}, function(){
		$(".animMenu", this).hide();
	});


	// Event lors du scroll
	$(window).scroll(function(e) {
		var y = $(this).scrollTop();
		var h = $("body").height();
		var wh = $(window).height();
		var p = Math.round(100*y/(h-wh));

		// Gère la position du menu
		if($("#menu").position().top <= y){
			$("#menu").addClass("posAbso");
			majPosAnimMenu();
		}
		if($("#trucZoli").height() > y){ // On replace le menu quand remonte au niveau du canvas
			$("#menu").removeClass("posAbso");
			majPosAnimMenu();
		}

		// Gère l'apparition de la flêche de retour en fonction du scroll
		var flecheRetMe = $("#flecheRetourMenu");
		if(p >= 30){
			if(flecheRetMe.hasClass("displaynone")){
				flecheRetMe.removeClass("displaynone")
				           .addClass("animated").addClass("swing");
				setTimeout(function(){
					flecheRetMe.removeClass("animated")
					           .removeClass("swing");
				}, 2000);
			}
		} else{
			if(!$flecheRetMe.hasClass("displaynone")){
				flecheRetMe.addClass("displaynone");
			}
		}

		// Chargement des loader quand ils passe à l'écran
		$(".loader").each(function(){
			var _this = $(this);							// loader
			var top = _this.position().top;					// top du loader
			var heightParent = _this.height();				// height du loader
			var load = _this.children(".load");				// element load (div rouge créer pour l'animtion)
			var loadWidth = load.width();					// widht de l'element load
			var valeur = _this.children("p").text();		// taille max du load (avec %)
				valeur = valeur.substr(0, valeur.length - 1);	// taille max du load (sans %)
			var enAnim = false;
			

			// Si le loader apparaît sur l'écran on lance l'animation.
			if(top <= y + wh - heightParent && top > y + 0.9){
				
				

				if(loadWidth == 0 && !enAnim){
					enAnim = true;
					for(var i = 1; i <= valeur; i++){
						animLoad(load, i);
					}
				}
			}else{ // Sinon on remet la valeur du load à 0, permet de relancer l'animation si les loader sont de nouveau sur l'écrans
				if(loadWidth >= valeur - 1){
					load.css("width", "0%");
					enAnim = false;
				}
			}
		});
		// Fonction pour l'animation du load 
		function animLoad(element, i){
			setTimeout(function(){
				element.css("width", i + "%");
			}, 8 * (i - 1));
		}
	});


	// Loader
	$(".loader").each(function(){
		// On récupère la valeur (pourcentage) du loader
		var valeur = $(this).text();
		$(this).children("p").append("%");

		$(this).css("height", $(this).parent().height());

		$(this).append('<div class="load" style="height: ' + $(this).height() + 'px; width: 0%;"></div');
		$(this).children("p").css("left", (valeur * 45/100) +"%");

		$(this).hover(function(){
			$(this).children("p").show()
			                     .addClass("animated")
			                     .addClass("bounceIn");
		}, function(){
			hidePourcentage($(this).children("p"));
		});


	});

		function hidePourcentage(element){
		setTimeout(function(){
			$(element).hide()
			          .addClass("animated").addClass("bounceIn");
		}, 100);
	}

	$("#flecheRetourMenu").hover(function(){
		$(this).addClass("animated").addClass("tada");
	}, function(){
		$(this).removeClass("animated").removeClass("tada");
	});

	function animMenu(element, width){
		setTimeout(function(){
			$(".animMenu", element).css("width", width + "px");
		}, 1.5 * width);
	}

	function animMonNom(width){
		setTimeout(function(){
			$("#monnom").css("width", width + "px");
		}, 2 * width + 500);
	}

	// Définit l'annimation du hover des logo FB et IN
	animHoverLogo();

	// Retour vers le haut de la page
	$("#flecheRetourMenu").click(function(){
		var topToGo = $("#personalData").position().top;
		var topWind = $(window).scrollTop();
		
		for(var i = topToGo; i < topWind; i += 5){
			bougeScroolTop(i - topToGo);
		}

		function bougeScroolTop(s){
			setTimeout(function(){
				var st = $(window).scrollTop();
				$(window).scrollTop(st - 5);
			}, 0.3 * s);
		}
	});

	// Aller vers le menu quand on click sur la première fleche
	$("#flecheVersMenu").click(function(){
		var topToGo = $("#personalData").position().top;
		var topWind = $(window).scrollTop();
		
		for(var i = topWind; i < topToGo - 30; i += 1){
			bougeScroolTop(i - topWind);
		}

		function bougeScroolTop(s){
			setTimeout(function(){
				var st = $(window).scrollTop();
				$(window).scrollTop(st + 1);
			}, s);
		}
	});
	


});



$(window).resize(function(){
	// Redéfinit la taille du texte du menu en fonction de la taille de la fenêtre
	$("#listMenu a").css("font-size", ($(window).width() * 0.016) + "px");
	// prépare les animation du survol des boutons du menu
	majPosAnimMenu();
});

function majPosAnimMenu() {
	// Met a jour la position des div utiliser pour l'animation hover sur le menu
	$(".animMenu").each(function(){
		var height = 0;
		// on récupère la position de l'element parent
		var position = $(this).parent().position();
		// On recupère la taille de l'element parent avec les marges
		height = $(this).parent().outerHeight();

		$(this).css("height", height + "px");

		$(this).css("top", position.top + "px");
		$(this).css("left", position.left + "px");
	});
}

// Fonction qui créer l'animation du canvas (les bulles)
function animFond(){
	var canvas = document.getElementById("trucZoli");
	var context = canvas.getContext("2d");
	var listBull = [];
	// Met à jour la taille du canvas
	majTailleCanvas();
	
	// Création de 50 bulles
	for(var i = 0; i < 50; i ++){
		var bull = new Bull(canvas);// Créer la bulle
		bull.init();				// Initialize les valeurs de la bulle
		listBull.push(bull);		// Ajoute la bulle créer à la liste de bulle
	}
	// On met à jour la position des bulles toute les 10 ms
	setInterval(uneBoulle, 10);

	function uneBoulle(){
		context.clearRect(0, 0, canvas.width, canvas.height);
		for(var i = 0; i < listBull.length; i++){ // Pour chaque bulle
			listBull[i].afficher(context); 		  // On l'affiche
			listBull[i].deplace();			      // Et on le déplace
		}
	}
	$(window).resize(function(){
		// Met à jour la taille du canvas
		majTailleCanvas();
		for(var i = 0; i < listBull.length; i++)
			listBull[i].majTailleMax(); // Met à jour les tailles max de chaque Bull
		
	});
}

// Fonction qui met à jour la taille du canvas
function majTailleCanvas(){
	var canvas = document.getElementById("trucZoli");
	// Définit la taille du canvas à la taille de la fenêtre
	canvas.setAttribute("height", $(window).height() + "px");
	canvas.setAttribute("width", $(window).width() + "px");
}

// Fonction pour l'animation lors d'un hover des logo facebook et linkdin
function animHoverLogo(){
	$(".logo").hover(function(){
		// Souris sur un logo
		var a = 0.5;			// alpha de backroud color
		var t = 1;				// taille pour le margin et le padding du logo
		var aAdd = 0.05;		// valeur à ajouter à l'alpha (a) à chaque tour de boucle
		var tAdd = 1;			// valeur à ajouter à la taille (t) à chaque tour de boucle
		var tps = 1;			// variable pour le temps du setTimeout

		while(a >= 0.5){
			colorBackgroundlogo($(this), a, tps);
			majPaddingMarginlogo($(this), t, tps);
			a += aAdd;
			t += tAdd;
			tps++;

			if(a >= 1){ // quand l'anim à fini de rétresir, on la fait grosire
				aAdd *= -1;
				tAdd *= -1;
			}
		}
	}, function(){
		// Souris sort du logo
		var element = $(this);
		setTimeout(function(){
			element.css("background-color", "");
		}, 200);
	});
}
// Fonction pour définir le background du logo (avec alpha)
function colorBackgroundlogo(element, a, tps){
	setTimeout(function(){
		element.css("background-color", "rgba(192,57,43, " + a + ")");
	}, 10 * tps);
}
// Fonction pour définir le margin et la padding du logo
function majPaddingMarginlogo(element, t, tps){
	setTimeout(function(){
		element.css("padding", (5 - (t/2)) + "px");
		element.css("margin", (t/2) + "px");
	}, 10 * tps);
}

