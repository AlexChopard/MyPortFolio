<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Alex CHOPARD</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/animate.css" />
	</head>
	<body>
		<p id='wip'>work in progress !</p>
		<div id="haut">
			<div id="monnom">
				<img id="mouton" src="./img/mouton.png">
				<h1>ALEX CHOPARD</h1>
			</div>

			<img id="flecheVersMenu" src="./img/fleche.jpg">
			
			<canvas id="trucZoli" width="500" height="2000"></canvas>	
		</div>


		
		<nav id="menu">

			<ul id="listMenu">
				<li class="unMenu"><div class="animMenu"></div><a href="#personalData">ME</a></li>
				<li class="unMenu"><div class="animMenu"></div><a href="#jobObjective">OBJECTIVE</a></li>
				<li class="unMenu"><div class="animMenu"></div><a href="#technicalSkills">SKILLS</a></li>
				<li class="unMenu"><div class="animMenu"></div><a href="#workExperiences">EXPERIENCES</a></li>
				<li class="unMenu"><div class="animMenu"></div><a href="#miscellaneous">MISCELLANEOUS</a></li>
				<li class="unMenu"><div class="animMenu"></div><a href="#languages">LANGUAGES</a></li>
				<li class="unMenu"><div class="animMenu"></div><a href="#educationQualifications">QUALIFICATIONS</a></li>
			</ul>

		</nav>

		<article id="personalData" class="content">
			<h2>Personal data</h2>

			<p>Alex CHOPARD</p>
			<p>27 Feb 1998</p>
			<p>2098 route de la Chapelle 74800 ETEAUX, France</p>
			<p>+33(0) 6 40 27 97 44</p>
			<p>alex.chopard1998@gmail.com</p>
		</article>

		<article id="jobObjective" class="content">
			<h2>Job Objective</h2>

			<p>I am a second-year university student in computer science. I am a good programmer in serial language, and with different technology. I am passionate in computer-science and I will always learn in it.</p>

		</article>


		<article id="technicalSkills" class="content">
			<h2>Personnal Skills</h2>
			<ul>
				<li>I am self-directed and able to take initiative.</li>
				<li>I am innovative , dynamic and confident.</li>
				<li>I am very task-oriented and focused.</li>
			</ul>

			<h2>Technical Skills</h2>

			<h3>Programming languages :</h3>
			<div class="conteneurLoader">
				<p>C</p>
				<div class="loader"><p>80</p></div>
			</div>
			<div class="conteneurLoader">
				<p>C++</p>
				<div class="loader"><p>95</p></div>
			</div>
			<div class="conteneurLoader">
				<p>C#</p>
				<div class="loader"><p>80</p></div>
			</div>
			<div class="conteneurLoader">
				<p>HTML</p>
				<div class="loader"><p>95</p></div>
			</div>
			<div class="conteneurLoader">
				<p>CSS</p>
				<div class="loader"><p>50</p></div>
			</div>
			<div class="conteneurLoader">
				<p>JS</p>
				<div class="loader"><p>90</p></div>
			</div>
			<div class="conteneurLoader">
				<p>PHP</p>
				<div class="loader"><p>100</p></div>
			</div>
			<div class="conteneurLoader">
				<p>Python</p>
				<div class="loader"><p>40</p></div>
			</div>

			<h3>Data Base :</h3>
			<div class="conteneurLoader">
				<p>PostgerSQL</p>
				<div class="loader"><p>90</p></div>
			</div>
			<div class="conteneurLoader">
				<p>Oracle</p>
				<div class="loader"><p>80</p></div>
			</div>
			<div class="conteneurLoader">
				<p>MySQL</p>
				<div class="loader"><p>80</p></div>
			</div>

			<h3>Development Environment :</h3>
			<div class="conteneurLoader">
				<p>Code::Blocks</p>
				<div class="loader"><p>90</p></div>
			</div>
			<div class="conteneurLoader">
				<p>Visual Studio</p>
				<div class="loader"><p>70</p></div>
			</div>
			<div class="conteneurLoader">
				<p>Eclipse</p>
				<div class="loader"><p>85</p></div>
			</div>
			<div class="conteneurLoader">
				<p>Android Studio</p>
				<div class="loader"><p>95</p></div>
			</div>
			<div class="conteneurLoader">
				<p>Spider</p>
				<div class="loader"><p>50</p></div>
			</div>

			<h3>Operating System :</h3>
			<div class="conteneurLoader">
				<p>Windows 10</p>
				<div class="loader"><p>100</p></div>
			</div>
			<div class="conteneurLoader">
				<p>Ubuntu</p>
				<div class="loader"><p>70</p></div>
			</div>
			<div class="conteneurLoader">
				<p>unix</p>
				<div class="loader"><p>70</p></div>
			</div>
			<div class="conteneurLoader">
				<p>Rasbian</p>
				<div class="loader"><p>90</p></div>
			</div>

			<h3>Framework :</h3>
			<div class="conteneurLoader">
				<p>Symfony 3</p>
				<div class="loader"><p>95</p></div>
			</div>

			<div class="conteneurLoader">
				<p>Unity</p>
				<div class="loader"><p>45</p></div>
			</div>



		</article>


		<article id="workExperiences" class="content">
			<h2>Work Experiences</h2>

		</article>

		<article id="miscellaneous" class="content">
			<h2>Miscellaneous</h2>

			<div class="hobbies">
				<h3>Climbing</h3>
				<p>I started Climbing in 2013 with high school. I very quickly progress until to have a good level (Currently I climb 7a). In 2016, I participated at the <a href="http://www.ugsel.org/wp-content/uploads/2016/05/R%C3%A9sultats-Bloc-2016.pdf" target="_blank">UGSEL France championship</a>, I was ranked 3 in block category and 4 in general. And in 2015, I participated at the UGSEL France championship, but not ranked.</p>
			</div>

			<div class="hobbies">	
				<h3>Tennis</h3>
				<p>I did 10 years of tennis before the high school in a club. In this years, I made serveral championship and, in my best, I was ranked 35.</p>
			</div>
		</article>

		<article id="languages" class="content">
			<h2>Languages</h2>	

			<div class="conteneurLoader">
				<p>French</p>
				<p>(native)</p>
				<div class="loader"><p>95</p></div>
			</div>
			<div class="conteneurLoader">
				<p>English</p>
				<div class="loader"><p>50</p></div>
			</div>
			<div class="conteneurLoader">
				<p>Spanish</p>
				<div class="loader"><p>20</p></div>
			</div>


		</article>

		<article id="educationQualifications" class="content">
			<h2>Education and Qualifications</h2>

			<div class="uneEduQual">
				<p class="date">2018-</p>
				<div class="contenueEduQual">
					<p class="diplometext">DUT Informatique (2-year university diploma in computer science).</p>
					<p class="lieu"><a href="https://www.univ-smb.fr/" target="_blank">Université Savoie Mont Blanc</a> / <a href="https://www.iut-acy.univ-smb.fr/" target="_blank">IUT Annecy</a> - Annecy-le-Vieux, France</p>
				</div>
				
			</div>

			<div class="uneEduQual">
				<p class="date">2016-</p>
				<div class="contenueEduQual">
					<p class="diplometext">Frensh Baccalaureate S (High School diploma specialized in science) with honors.</p>
					<p class="lieu"><a href="http://www.escr74.com/" target="_blank">ESCR Sainte-Famille</a> High school - La Roche-sur-Foron, France</p>	
				</div>
			</div>

		</article>

		<article id="references" class="content">
			<h2>References</h2>			

		</article>

		<img id="flecheRetourMenu" class="displaynone" src="./img/flecheHaut.png">
	
	<script src='js/jquery.js'></script>
	<script src='js/main.js'></script>
	<script src='js/Bulle.js'></script>
	
	<footer>
		<p>Alex CHOPARD © 2017</p>
		<a href="https://www.facebook.com/alex.chopard.7" target="_blank"><img id="logo_fb" class="logo" src="./img/fa.png"></a>
		<a href="https://www.linkedin.com/in/alex-chopard-383781133/" target="_blank"><img id="logo_in" class="logo" src="./img/logo_in.png"></a>
	</footer>

	</body>
</html>
